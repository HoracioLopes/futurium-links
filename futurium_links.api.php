<?php
/**
 * @file
 * Hooks provided by the Organic groups module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the autocomplete query.
 */
function hook_fl_autocomplete_query_alter($query) {

}

/**
 * Alter the autocomplete matches.
 */
function hook_fl_autocomplete_matches_alter($matches) {

}

/**
 * Alter the autocomplete label variables.
 */
function hook_fl_autocomplete_label_alter($label_vars) {

}
